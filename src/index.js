import _ from 'lodash';
import './style.css';
import 'svg.js';

let w   = window.innerWidth,
    h   = window.innerHeight,
    s   = _.max([w, h]),
    δr  = randomNormal(2000, 100),
    sr  = randomNormal(12000, 100),
    δx  = randomNormal(s/2, s),
    δy  = randomNormal(s/2, s),
    r   = randomNormal(0, 1),

    draw    = SVG('frame'),
    bg      = draw.rect(w, h).fill('#fff'),
    group   = draw.group(),
    text    = draw.text(function(add) { add.tspan( "Laura Pirgie" ) }),
    
    shapes = []

const BLEND_MODES = [ 'normal',
                      'multiply',
                      'screen',
                      'overlay',
                      'darken',
                      'lighten',
                      'color-dodge',
                      'color-burn',
                      'hard-light',
                      'soft-light',
                      'difference']

const MIN_SHAPES = 6
const MAX_SHAPES = 24
var numShapes = _.random(MIN_SHAPES, MAX_SHAPES)

text
  .path('M24 292 C 249 291, 393 96, 602 96')
  .animate(1400, '<>')
  .plot('M24 292 C 234 292, 412 409, 595 401')
  .loop(true, true)

// start drawing shapes
makeShapeIntervalFn();
updateNumShapesIntervalFn();


function makeShapeIntervalFn() {

  var rect = draw.polygon(makeShape())
        .fill(getRandomColor())
        .style('mix-blend-mode', _.sample(BLEND_MODES))
  group.add(rect)
  shapes.push(rect)

  if(shapes.length > numShapes) {
    let head = shapes.shift()
    head.remove()
  }

  // reschedule
  setTimeout(makeShapeIntervalFn, δr());
}

function updateNumShapesIntervalFn() {
  numShapes = _.random(MIN_SHAPES, MAX_SHAPES)
  console.log(`numShapes: ${numShapes}`)
  // reschedule
  setTimeout(updateNumShapesIntervalFn, sr());
}

function makeShape() {
  // let numPoints = r() < 0 ? 3 : 4
  let numPoints = 3

  let coordinates = _(numPoints)
                      .range()
                      .map(() => [δx(true), δy(true)])
                      .value()
  return coordinates
}


function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

// normal random function lifted from d3-random
// https://github.com/d3/d3-random/blob/master/src/normal.js
function randomNormal(mu, sigma) {
    var x, r;
    mu = mu == null ? 0 : +mu;
    sigma = sigma == null ? 1 : +sigma;
    return function(round) {
      var y;

      // If available, use the second previously-generated uniform random.
      if (x != null) y = x, x = null;

      // Otherwise, generate a new x and y.
      else do {
        x = Math.random() * 2 - 1;
        y = Math.random() * 2 - 1;
        r = x * x + y * y;
      } while (!r || r > 1);

      let result = mu + sigma * y * Math.sqrt(-2 * Math.log(r) / r);
      if(round) return Math.round(result)
      else return result
    };
  }


